/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {View,FlatList,Image, ActivityIndicator } from 'react-native';
import { Body,Button, Title,Item, Input, Container, Header, Content, List, ListItem, Text, Left, Right, Icon } from 'native-base';
import _ from 'lodash';
import Axios from 'axios';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import PDFView from 'react-native-view-pdf';

class Dokumen extends Component{
  constructor(props) {
		super(props);
		this.state = {
      data : [],
		}
  }
  
  render(){
    return(
      <Container>
        <Header style={{backgroundColor:'#000'}}>
          <Body>
            <Title style={{ textTransform: 'uppercase', fontSize:14}}>{this.props.navigation.getParam('Dokumen')}</Title>
          </Body>
          <Right>
            <Button transparent  onPress={()=>this.props.navigation.goBack()}>
              <Text>Keluar</Text>
            </Button>
          </Right>
        </Header>
        
        <View style={{ flex: 1 }}>
          <PDFView
            style={{ flex: 1 }}
            onError={(error) => console.log('onError', error)}
            onLoad={() => console.log('PDF rendered from url')}
            resource={this.props.navigation.getParam('urlDownload')}
            resourceType="url"
          />
        </View>
      </Container>
      
    )
  }
}

class ListDokumen extends Component{
  constructor(props) {
		super(props);
		this.state = {
      data : [],
      search :''
		}
  }
  componentDidMount(){
    var data = this.props.navigation.getParam('data');
    data.sort(function(a, b) {
        return a.judul < b.judul;
    });
    this.setState({
      data : data
    })
    this.filterData =data;
  }

  SearchFilterFunction(text) {
    const newData = this.filterData.filter((item) =>{
      const itemData = item.judul ? item.judul.toUpperCase() : ''.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      data: newData,
      search: text,
    })
  }
  
  render(){
    return(
      <Container>
        <Header style={{backgroundColor:'#000'}}>
          <Left>
            <Button transparent onPress={()=>this.props.navigation.goBack()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>{this.props.navigation.getParam('jenis')}</Title>
          </Body>
        </Header>
        <Header searchBar rounded  style={{backgroundColor:'#000'}}>
          <Item>
            <Icon name="ios-search" />
            <Input placeholder="Cari berdasarkan Judul"
                onChangeText={text => {this.setState({search:text});this.SearchFilterFunction(text)}}
                onClear={text => this.SearchFilterFunction('')}
                value={this.state.search}
             />
            <Icon name="eye" />
          </Item>
        </Header>
        <Content>
          
          <List>
            <FlatList
              data={this.state.data}
              renderItem={({item})=>{
                var dok = item.singkatanJenis+ ' No.'+item.noPeraturan+' Tahun ' + item.tahun_pengundangan
                return(
                  <ListItem button={true} onPress={()=>{this.props.navigation.navigate('Dokumen', {urlDownload :item.urlDownload, Dokumen : dok})}}>
                    <Body>
                      <Text style={{ textTransform: 'uppercase'}}>{item.singkatanJenis} No. {item.noPeraturan} Tahun {item.tahun_pengundangan}</Text>
                      <Text style={{fontSize:10}} note>{item.judul}</Text>
                      
                    </Body>
                    <Right>
                      <Icon name="eye" />
                    </Right>
                  </ListItem>
                )
              }}
            />
            
          </List>
        </Content>
      </Container>
    )
  }
}

class Home extends Component {
  constructor(props) {
		super(props);
		this.state = {
      jenis : [],
      fetching : true,
		}
  }

  componentDidMount(){
    Axios.get('http://www.jdih.butonkab.go.id/integrasi.php')
    .then((response)=>{
      console.log(response.data);

      const result = _.chain(response.data)
			.groupBy("jenis")
			.toPairs()
			.map(pair => _.zipObject(['jenis', 'data'], pair))
      .value();
      result.sort(function(a, b) {
          return a.data.length < b.data.length;
      });
      this.setState({
        jenis : result,
        fetching : false
      })
    })
  }
  
  render() {
    return (
      <Container>
        <Header style={{backgroundColor:'#000'}}>
          <Left>
            <Image source={require('./Assets/kabbuton.png')} style={{height:40, width:30}}/>
          </Left>
          <Body>
            
            <Title>JDIH Kabupaten Buton</Title>
          </Body>
        </Header>
        <Content>
          {
            this.state.fetching ? 
            <View style={{flex: 1, padding: 20, alignItems:'center'}}>
              <ActivityIndicator/>
              <Text>Sedang memuat data</Text>
            </View> 
            :             
            <List>              
              <FlatList
                data={this.state.jenis}
                renderItem={({item})=>{
                  return(
                    <ListItem button={true} onPress={()=>{this.props.navigation.navigate('ListDokumen', {jenis :item.jenis, data : item.data})}}>
                      <Left>
                        <Icon name='paper'/>
                        <Text>{item.jenis}</Text>
                      </Left>
                      <Right>
                        <Text>{item.data.length}</Text>
                      </Right>
                    </ListItem>
                  )
                }}
              />              
            </List>        
          }
          </Content>
      </Container>
      );
  };
}

const MainNavigator = createStackNavigator({
  Home: {screen: Home},
  ListDokumen: {screen: ListDokumen},
  Dokumen: {screen: Dokumen},
},{
  headerMode: 'none',
  initialRouteName: 'Home'
});
const AppContainer = createAppContainer(MainNavigator);
export default class App extends React.Component {  

  render() {
    return (
      <AppContainer/>
    )
  }
}


